package ro.tuc.tp.tema1;

public interface IOperation {

	public static IOperation getInstance(String s) {

		switch (s) {
		case "+":
			return new AddOp();
		case "-":
			return new SubOp();
		case "x":
			return new MulOp();
		//case "/":
		//	return new DivOp();
		case "derivare":
			return new DerOp();
		case "integrare":
			return new IntOp();
		default:
			return null;
		}
	}

	public Polinom compute(Polinom p1, Polinom p2);
}
