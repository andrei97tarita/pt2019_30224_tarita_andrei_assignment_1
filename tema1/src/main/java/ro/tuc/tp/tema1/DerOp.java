package ro.tuc.tp.tema1;

public class DerOp implements IOperation {

	@Override
	public Polinom compute(Polinom p1, Polinom p2) {
		// TODO Auto-generated method stub

		for (Monom monom : p1.termsList) {
			Monom m = new Monom(monom.getCoeff() * monom.getDegree(), monom.getDegree() - 1);
			p2.addMonom(m);
		}

		return p2;
	}

}