package ro.tuc.tp.tema1;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.*;

public class GUI {

	public void metodaGUI() {

		JFrame frame = new JFrame("Operatii polinoame");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1500, 120);

		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();

		JLabel label1 = new JLabel("Polinom 1: ");
		JTextField tf1 = new JTextField(20);
		JLabel label2 = new JLabel("Polinom 2: ");
		JTextField tf2 = new JTextField(20);
		JLabel label3 = new JLabel("Operatie: ");
		JTextField tf3 = new JTextField(38);

		panel1.add(label1);
		panel1.add(tf1);
		panel1.add(label2);
		panel1.add(tf2);
		panel1.add(label3);

		JButton buttonAdd = new JButton("+");
		JButton buttonSub = new JButton("-");
		JButton buttonMul = new JButton("x");
		JButton buttonDiv = new JButton("/");
		
		buttonDiv.setEnabled(false);

		ArrayList<JButton> binaryOpBtns = new ArrayList<JButton>();

		binaryOpBtns.add(buttonAdd);
		binaryOpBtns.add(buttonSub);
		binaryOpBtns.add(buttonMul);
		binaryOpBtns.add(buttonDiv);

		for (JButton btn : binaryOpBtns) {
			panel1.add(btn);
		}

		JLabel label4 = new JLabel("Rezultat: ");

		panel1.add(label4);
		panel1.add(tf3);
		tf3.setEditable(false);

		JLabel label5 = new JLabel("Polinom: ");
		JTextField tf4 = new JTextField(20);

		panel2.add(label5);
		panel2.add(tf4);

		JButton buttonDerivare = new JButton("derivare");
		JButton buttonIntegrare = new JButton("integrare");

		ArrayList<JButton> unaryOpBtns = new ArrayList<JButton>();

		unaryOpBtns.add(buttonDerivare);
		unaryOpBtns.add(buttonIntegrare);

		panel2.add(label3);

		panel2.add(buttonDerivare);
		panel2.add(buttonIntegrare);

		JLabel label6 = new JLabel("Rezultat: ");

		panel2.add(label6);

		JTextField tf5 = new JTextField(35);
		panel2.add(tf5);
		tf5.setEditable(false);

		frame.getContentPane().add(BorderLayout.CENTER, panel2);
		frame.getContentPane().add(BorderLayout.NORTH, panel1);

		frame.setVisible(true);

		for (JButton btn : unaryOpBtns) {
			btn.addActionListener(e -> {
				Polinom p1 = new Polinom(tf4.getText());
				Polinom p2 = new Polinom("");

				p2 = IOperation.getInstance(btn.getText()).compute(p1, p2);
				tf5.setText(p2.toString());
			});
		}

		for (JButton btn : binaryOpBtns) {
			btn.addActionListener(e -> {
				Polinom p1 = new Polinom(tf1.getText());
				Polinom p2 = new Polinom(tf2.getText());
				Polinom p3 = new Polinom("");

				p3 = IOperation.getInstance(btn.getText()).compute(p1, p2);
				tf3.setText(p3.toString());
			});
		}

	}

}
