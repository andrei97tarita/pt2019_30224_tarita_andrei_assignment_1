package ro.tuc.tp.tema1;

public class Monom {

	private float coeff;
	private int degree;

	public int getDegree() {
		return degree;
	}

	public void setDegree(int degree) {
		this.degree = degree;
	}

	public float getCoeff() {
		return coeff;
	}

	public void setCoeff(float coeff) {
		this.coeff = coeff;
	}

	public Monom(float coeff, int degree) {
		// TODO Auto-generated constructor stub
		this.coeff = coeff;
		this.degree = degree;
	}
}
