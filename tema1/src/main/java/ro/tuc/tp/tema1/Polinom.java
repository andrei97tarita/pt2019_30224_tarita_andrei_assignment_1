package ro.tuc.tp.tema1;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polinom {

	ArrayList<Monom> termsList = new ArrayList<Monom>();

	public Polinom(String s) {
		Pattern pattern = Pattern.compile("(-?\\b\\d+)[xX]\\^(-?\\d+\\b)");
		Matcher matcher = pattern.matcher(s);
		while (matcher.find()) {
			termsList.add(new Monom(Float.parseFloat(matcher.group(1)), Integer.parseInt(matcher.group(2))));
		}
	}

	public void addMonom(Monom m) {

		boolean gasit = false;

		for (Monom monom : termsList) {
			if (monom.getDegree() == m.getDegree()) {
				monom.setCoeff(monom.getCoeff() + m.getCoeff());
				gasit = true;
				break;
			}
		}

		if (gasit == false) {
			termsList.add(m);
		}
	}

	public String toString() {

		String expresie = "";

		for (Monom monom : this.termsList) {

			if (monom.getCoeff() != 0)
				expresie = expresie + monom.getCoeff() + "x^" + monom.getDegree() + " + ";

		}
		if (expresie.endsWith(" + "))
			return expresie.substring(0, expresie.length() - 2);
		else
			return expresie;
	}

	public int getGradPolinom() {
		int max = 0;
		for (Monom m : this.termsList) {
			if (m.getCoeff() != 0.0 && m.getDegree() > max)
				max = m.getDegree();
		}
		return max;
	}

	public Monom getMaxMonom() {
		Monom maxMon = new Monom(0, 0);
		for (Monom m : this.termsList) {
			if (m.getDegree() == m.getDegree()) {
				if (m.getCoeff() >= m.getCoeff())
					maxMon = m;
			} else if (m.getDegree() > m.getDegree()) {
				maxMon = m;
			}
		}

		return maxMon;
	}

}
