package ro.tuc.tp.tema1;

import java.util.Collections;

public class SubOp implements IOperation {

	@Override
	public Polinom compute(Polinom p1, Polinom p2) {
		// TODO Auto-generated method stub
		Polinom p3 = p1;

		boolean ok = false;

		for (Monom m2 : p2.termsList) {
			ok = false;
			for (Monom m3 : p3.termsList) {
				if (m2.getDegree() == m3.getDegree()) {
					m3.setCoeff(m3.getCoeff() - m2.getCoeff());
					ok = true;
				}
			}

			if (ok == false) {
				m2.setCoeff(-(m2.getCoeff()));
				p3.addMonom(m2);
			}
		}

		Collections.sort(p3.termsList, new sortare());

		return p3;
	}

}
