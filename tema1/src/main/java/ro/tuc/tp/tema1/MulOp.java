package ro.tuc.tp.tema1;

import java.util.Collections;

public class MulOp implements IOperation {

	@Override
	public Polinom compute(Polinom p1, Polinom p2) {
		// TODO Auto-generated method stub

		Polinom p3 = new Polinom("");

		for (Monom m1 : p1.termsList) {
			for (Monom m2 : p2.termsList) {
				p3.addMonom(new Monom(m1.getCoeff() * m2.getCoeff(), m1.getDegree() + m2.getDegree()));
			}
		}

		Collections.sort(p3.termsList, new sortare());

		return p3;
	}
}
