package ro.tuc.tp.tema1;

import static org.junit.Assert.*;
import org.junit.Test;

public class MulOpTest {

	@Test
	public void test() {

		Polinom p1 = new Polinom("3x^2+2x^1");
		Polinom p2 = new Polinom("3x^1-2x^0");

		Polinom res3 = new MulOp().compute(p1, p2);

		assertEquals("9.0x^3 + -4.0x^1 ", res3.toString());
	}

}
