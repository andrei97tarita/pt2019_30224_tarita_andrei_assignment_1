package ro.tuc.tp.tema1;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AddOpTest.class, DivOpTest.class, IntOpTest.class, MulOpTest.class, SubOpTest.class })
public class AllTests {

}
