package ro.tuc.tp.tema1;

import static org.junit.Assert.*;
import org.junit.Test;

public class AddOpTest {

	@Test
	public void test() {

		Polinom p1 = new Polinom("3x^2+2x^1");
		Polinom p2 = new Polinom("3x^1-2x^0");

		Polinom res2 = new SubOp().compute(p1, p2);
		
		assertEquals("3.0x^2 + -1.0x^1 + 2.0x^0 ", res2.toString());
	}

}
