package ro.tuc.tp.tema1;

import static org.junit.Assert.*;
import org.junit.Test;

public class IntOpTest {

	@Test
	public void test() {

		Polinom p1 = new Polinom("3x^2+2x^1");
		Polinom p2 = new Polinom("");

		Polinom res5 = new IntOp().compute(p1, p2);

		assertEquals("1.0x^3 + 1.0x^2 ", res5.toString());
	}

}
