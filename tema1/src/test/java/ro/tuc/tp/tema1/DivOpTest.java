package ro.tuc.tp.tema1;

import static org.junit.Assert.*;
import org.junit.Test;

public class DivOpTest {

	@Test
	public void test() {

		Polinom p1 = new Polinom("3x^2+2x^1");
		Polinom p2 = new Polinom("");

		Polinom res4 = new DerOp().compute(p1, p2);

		assertEquals("6.0x^1 + 2.0x^0 ", res4.toString());
	}

}
